#!/usr/bin/env bash

REPOS="$1"
REV="$2"

# define required vars
python_path=python
svn_hook=`dirname $0`/jirahooks/svn_post_commit.py

# execute
${python_path} ${svn_hook} --config "svn_cfg.json" --repos ${REPOS} --rev ${REV}

exit 0