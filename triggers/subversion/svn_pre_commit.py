#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from json import load, loads, dumps
import argparse
import sys
import os


class SvnPreCommitHook(object):

    def __init__(self, config):
        """
        __init__(). prepares or initiates configuration file
        :return: None
        """

        if not config:  # default config file
            config = "svn_cfg.json"

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if self.config_file and os.path.exists(self.config_file):
            try:
                configuration = load(open(self.config_file))

                self.svn_look_cmd = configuration.get("svn.look.cmd.path")
                if not os.path.exists(self.svn_look_cmd):
                    self.svn_look_cmd = "svnlook"

                self.jch = JiraCommitHook(configuration)

            except ValueError, e:
                sys.stderr.write("{0}\n".format(e))
                sys.exit(1)
        else:
            sys.stderr.write("Config file ({0}) not exists!\n".format(config))
            sys.exit(1)

    def run(self, repository, txn):
        """
        run(). executes the jira update for the given change-set
        :param repository: Subversion repository path
        :param txn: Subversion txn
        :return: None
        """

        svn_rev_message = self.jch.command_output("{0} log -t {1} {2}".format(self.svn_look_cmd, txn, repository))

        if svn_rev_message:

            matched_issue_keys = self.jch.pattern_validate(svn_rev_message)

            if len(matched_issue_keys) != 0:
                self.jch.jira_pre_validate(matched_issue_keys.keys())
            else:
                sys.stderr.write( "[Error] *** Required at-least one JIRA Issue ID. e.g. [DUMMY-5] or (DUMMY-5).\n" )
                sys.exit(1)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    parser = argparse.ArgumentParser(description='SCM Activity GIT Post Receive Hook Execution Script')
    parser.add_argument("--config", help='Required config')
    parser.add_argument("--repos", help='Required repository', required=True)
    parser.add_argument("--txn", help='Required transaction', required=True)

    args = parser.parse_args()

    if args.repos and args.txn:
        g = SvnPreCommitHook(args.config)
        g.run(args.repos, args.txn)
    else:
        sys.stderr.write( "[usage] pre-commit repos txn\n" )
        sys.exit(1)


if __name__ == '__main__':
    main()
